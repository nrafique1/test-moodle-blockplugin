<?php

/**
 * Form for editing HTML block instances.
 *
 * @package   block_testblock
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_testblock extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_testblock');
    }

    function has_config() {
        return true;
    }
    function get_content() {
        global $DB;

        if ($this->content !== NULL) {
            return $this->content;
        }

        $content = '';

        $showcourses = get_config('block_testblock', 'showcourses');

        if ($showcourses) {
            $courses = $DB->get_records('course');        
            foreach ($courses as $course) {
                $content .= $course->fullname . '<br>';
            }
        } else {
            $users = $DB->get_records('user');
            foreach ($users as $user) {
                $content .= $user->firstname . ' ' . $user->lastname . '<br>';
            }
        }

        $this->content = new stdClass;
        $this->content->text = $content;
        $this->content->footer = 'this is the footer';
        return $this->content;
    }
}
